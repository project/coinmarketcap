<?php

/**
 * @file
 * Contains cmc_entity.page.inc.
 *
 * Page callback for Cmc entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Cmc entity templates.
 *
 * Default template: cmc_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_cmc_entity(array &$variables) {
  // Fetch CmcEntity Entity Object.
  $cmc_entity = $variables['elements']['#cmc_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
