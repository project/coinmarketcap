<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 20.11.18.
 * Time: 13:44
 */

namespace Drupal\coinmarketcap_globalmetric\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Site\Settings;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "coinmarketcap_globalmetric",
 *   admin_label = @Translation("Global metric"),
 *   category = @Translation("Custom")
 * )
 */
class GlobalMetric extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $url = 'https://pro-api.coinmarketcap.com/v1/global-metrics/quotes/latest';
    $parameters = [
    'convert' => Settings::get('cmc_default_convert', 'USD')
    ];

    $options['headers'] = [
    'Accepts' => 'application/json',
    'X-CMC_PRO_API_KEY' => Settings::get('cmc_api_key', NULL)
    ];
    $qs = http_build_query($parameters); // query string encode the parameters
    $request = "{$url}?{$qs}"; // create the request URL

    $client = \Drupal::httpClient();
    $response_body = $client->request('GET', $request, $options);
    $response = json_decode($response_body->getBody(), TRUE);
    $build =  [
      '#data' => $response["#data"]["active_cryptocurrencies"],
        '#markup' =>
        '<div class="active_cryptocurrencies" >Active Cryptocurrencies: '. $response["data"]["active_cryptocurrencies"] .'</div>'.
        '<div class="total_cryptocurrencies" >Total Cryptocurrencies: '. $response["data"]["total_cryptocurrencies"] .'</div>'.
        '<div class="active_market_pairs" >Active market pairs: '. $response["data"]["active_market_pairs"] .'</div>'.
        '<div class="active_exchanges" >Active exchanges: '. $response["data"]["active_exchanges"] .'</div>'.
        '<div class="total_exchanges" >Total exchanges: '. $response["data"]["total_exchanges"] .'</div>'.
        '<div class="eth_dominance" >Eth dominance: '. $response["data"]["eth_dominance"] .'</div>'.
        '<div class="btc_dominance" >Btc dominance: '. $response["data"]["btc_dominance"] .'</div>'.
        '<div class="total_market_cap" >Total market cap: '. $response["data"]["quote"]["USD"]["total_market_cap"] .'</div>'.
        '<div class="total_volume_24h" >Total volume 24h: '. $response["data"]["quote"]["USD"]["total_volume_24h"] .'</div>'.
        '<div class="total_volume_24h_reported" >Total volume 24h reported: '. $response["data"]["quote"]["USD"]["total_volume_24h_reported"] .'</div>'.
        '<div class="altcoin_volume_24h" >Altcoin volume 24h: '. $response["data"]["quote"]["USD"]["altcoin_volume_24h"] .'</div>'.
        '<div class="altcoin_volume_24h_reported" >Altcoin volume 24h reported: '. $response["data"]["quote"]["USD"]["altcoin_volume_24h_reported"] .'</div>'.
        '<div class="altcoin_market_cap" >Altcoin market cap: '. $response["data"]["quote"]["USD"]["altcoin_market_cap"] .'</div>'.
        '<div class="last_updated" >Last updated: '. $response["data"]["last_updated"] .'</div>'
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }
}
