<?php

namespace Drupal\coinmarketcap;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Cmc entity entity.
 *
 * @see \Drupal\coinmarketcap\Entity\CmcEntity.
 */
class CmcEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\coinmarketcap\Entity\CmcEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished cmc entity entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published cmc entity entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit cmc entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete cmc entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add cmc entity entities');
  }

}
