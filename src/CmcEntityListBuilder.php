<?php

namespace Drupal\coinmarketcap;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Cmc entity entities.
 *
 * @ingroup coinmarketcap
 */
class CmcEntityListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Cmc entity ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\coinmarketcap\Entity\CmcEntity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.cmc_entity.edit_form',
      ['cmc_entity' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
