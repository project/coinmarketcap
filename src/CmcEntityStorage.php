<?php

namespace Drupal\coinmarketcap;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\coinmarketcap\Entity\CmcEntityInterface;

/**
 * Defines the storage handler class for Cmc entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Cmc entity entities.
 *
 * @ingroup coinmarketcap
 */
class CmcEntityStorage extends SqlContentEntityStorage implements CmcEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(CmcEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {cmc_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {cmc_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(CmcEntityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {cmc_entity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('cmc_entity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
