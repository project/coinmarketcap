<?php

namespace Drupal\coinmarketcap;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\coinmarketcap\Entity\CmcEntityInterface;

/**
 * Defines the storage handler class for Cmc entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Cmc entity entities.
 *
 * @ingroup coinmarketcap
 */
interface CmcEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Cmc entity revision IDs for a specific Cmc entity.
   *
   * @param \Drupal\coinmarketcap\Entity\CmcEntityInterface $entity
   *   The Cmc entity entity.
   *
   * @return int[]
   *   Cmc entity revision IDs (in ascending order).
   */
  public function revisionIds(CmcEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Cmc entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Cmc entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\coinmarketcap\Entity\CmcEntityInterface $entity
   *   The Cmc entity entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(CmcEntityInterface $entity);

  /**
   * Unsets the language for all Cmc entity with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
