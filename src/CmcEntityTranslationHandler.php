<?php

namespace Drupal\coinmarketcap;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for cmc_entity.
 */
class CmcEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
