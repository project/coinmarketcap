<?php

namespace Drupal\coinmarketcap\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\coinmarketcap\Entity\CmcEntityInterface;

/**
 * Class CmcEntityController.
 *
 *  Returns responses for Cmc entity routes.
 */
class CmcEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Cmc entity  revision.
   *
   * @param int $cmc_entity_revision
   *   The Cmc entity  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($cmc_entity_revision) {
    $cmc_entity = $this->entityManager()->getStorage('cmc_entity')->loadRevision($cmc_entity_revision);
    $view_builder = $this->entityManager()->getViewBuilder('cmc_entity');

    return $view_builder->view($cmc_entity);
  }

  /**
   * Page title callback for a Cmc entity  revision.
   *
   * @param int $cmc_entity_revision
   *   The Cmc entity  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($cmc_entity_revision) {
    $cmc_entity = $this->entityManager()->getStorage('cmc_entity')->loadRevision($cmc_entity_revision);
    return $this->t('Revision of %title from %date', ['%title' => $cmc_entity->label(), '%date' => format_date($cmc_entity->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Cmc entity .
   *
   * @param \Drupal\coinmarketcap\Entity\CmcEntityInterface $cmc_entity
   *   A Cmc entity  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(CmcEntityInterface $cmc_entity) {
    $account = $this->currentUser();
    $langcode = $cmc_entity->language()->getId();
    $langname = $cmc_entity->language()->getName();
    $languages = $cmc_entity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $cmc_entity_storage = $this->entityManager()->getStorage('cmc_entity');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $cmc_entity->label()]) : $this->t('Revisions for %title', ['%title' => $cmc_entity->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all cmc entity revisions") || $account->hasPermission('administer cmc entity entities')));
    $delete_permission = (($account->hasPermission("delete all cmc entity revisions") || $account->hasPermission('administer cmc entity entities')));

    $rows = [];

    $vids = $cmc_entity_storage->revisionIds($cmc_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\coinmarketcap\CmcEntityInterface $revision */
      $revision = $cmc_entity_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $cmc_entity->getRevisionId()) {
          $link = $this->l($date, new Url('entity.cmc_entity.revision', ['cmc_entity' => $cmc_entity->id(), 'cmc_entity_revision' => $vid]));
        }
        else {
          $link = $cmc_entity->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.cmc_entity.translation_revert', ['cmc_entity' => $cmc_entity->id(), 'cmc_entity_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.cmc_entity.revision_revert', ['cmc_entity' => $cmc_entity->id(), 'cmc_entity_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.cmc_entity.revision_delete', ['cmc_entity' => $cmc_entity->id(), 'cmc_entity_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['cmc_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
