<?php

namespace Drupal\coinmarketcap\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Cmc entity entity.
 *
 * @ingroup coinmarketcap
 *
 * @ContentEntityType(
 *   id = "cmc_entity",
 *   label = @Translation("Cmc entity"),
 *   handlers = {
 *     "storage" = "Drupal\coinmarketcap\CmcEntityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\coinmarketcap\CmcEntityListBuilder",
 *     "views_data" = "Drupal\coinmarketcap\Entity\CmcEntityViewsData",
 *     "translation" = "Drupal\coinmarketcap\CmcEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\coinmarketcap\Form\CmcEntityForm",
 *       "add" = "Drupal\coinmarketcap\Form\CmcEntityForm",
 *       "edit" = "Drupal\coinmarketcap\Form\CmcEntityForm",
 *       "delete" = "Drupal\coinmarketcap\Form\CmcEntityDeleteForm",
 *     },
 *     "access" = "Drupal\coinmarketcap\CmcEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\coinmarketcap\CmcEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "cmc_entity",
 *   data_table = "cmc_entity_field_data",
 *   revision_table = "cmc_entity_revision",
 *   revision_data_table = "cmc_entity_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer cmc entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/cmc_entity/{cmc_entity}",
 *     "add-form" = "/admin/structure/cmc_entity/add",
 *     "edit-form" = "/admin/structure/cmc_entity/{cmc_entity}/edit",
 *     "delete-form" = "/admin/structure/cmc_entity/{cmc_entity}/delete",
 *     "version-history" = "/admin/structure/cmc_entity/{cmc_entity}/revisions",
 *     "revision" = "/admin/structure/cmc_entity/{cmc_entity}/revisions/{cmc_entity_revision}/view",
 *     "revision_revert" = "/admin/structure/cmc_entity/{cmc_entity}/revisions/{cmc_entity_revision}/revert",
 *     "revision_delete" = "/admin/structure/cmc_entity/{cmc_entity}/revisions/{cmc_entity_revision}/delete",
 *     "translation_revert" = "/admin/structure/cmc_entity/{cmc_entity}/revisions/{cmc_entity_revision}/revert/{langcode}",
 *     "collection" = "/admin/structure/cmc_entity",
 *   },
 *   field_ui_base_route = "cmc_entity.settings"
 * )
 */
class CmcEntity extends RevisionableContentEntityBase implements CmcEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the cmc_entity owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }
    /**
     * {@inheritdoc}
     */
    public function getDateAdded() {
        return $this->get('date_added')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setDateAdded($date_added) {
        $this->set('date_added', $date_added);
        return $this;
    }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Cmc entity entity.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 48,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Cryptocurrency.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['cmc_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('CMC ID'))
      ->setDescription(t('ID of CMC'))
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => -4,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['date_added'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Date added'))
      ->setDescription(t('Date cryptocurrency was added to the system'))
      ->setSetting('datetime_type', 'date')
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'datetime',
          'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => 8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['ticker'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Ticker'))
      ->setDescription(t('The ticker of the Cryptocurrency'))
      ->addConstraint('UniqueField')
      ->setSettings([
        'max_length' => 10,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

      $fields['slug'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Slug'))
      ->setDescription(t('The slug of the Cryptocurrency'))
      ->addConstraint('UniqueField')
      ->setSettings([
        'max_length' => 20,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['price'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Price'))
      ->setDescription(t('The price of the Cryptocurrency.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 10,
        'prefix' => '$',
      ])
      ->setDefaultValue('0')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number',
        'weight' => -2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number_decimal',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

      $fields['cmc_rank'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('CMC rank'))
      ->setDescription(t('Market cap ranking of the Cryptocurrency'))
      ->setRevisionable(TRUE)
      ->setSetting('max_length', 10)
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -1,
        'settings' => array(
          'display_label' => TRUE,
        ),
        ))
      ->setDisplayOptions('view', array(
          'label' => 'above',
          'type' => 'number_integer',
          'weight' => -2,
        ))
        ->setDisplayConfigurable('form', TRUE)
        ->setRequired(TRUE);

    $fields['percent_change_1h'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Price change 1h'))
      ->setDescription(t('Cryptocurrency percent change in 1h.'))
      ->setSettings([
        'max_length' => 10,
        'suffix' => '%',
      ])
      ->setDefaultValue('0')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number',
        'weight' => -2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number_decimal',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['percent_change_24h'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Price change 24h'))
      ->setDescription(t('Cryptocurrency percent change in 24h.'))
      ->setSettings([
        'max_length' => 10,
        'suffix' => '%',    
      ])
      ->setDefaultValue('0')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number',
        'weight' => -2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number_decimal',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['percent_change_7d'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Price change in 7 days'))
      ->setDescription(t('Cryptocurrency percent change in 7 days.'))
      ->setSettings([
        'max_length' => 10,
        'suffix' => '%',      
      ])
      ->setDefaultValue('0')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number',
        'weight' => -2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number_decimal',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

     $fields['circulating_supply'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Circulating supply'))
      ->setRevisionable(TRUE)      
      ->setDescription(t('Circulating supply of the Cryptocurrency'))
      ->setSettings([
           'unsigned' => true,
           'size' => 'big'
         ])
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -1,
        'settings' => array(
          'display_label' => TRUE,
        ),
      ))
     ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => -2,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(FALSE);

     $fields['total_supply'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total supply'))
      ->setRevisionable(TRUE)      
      ->setDescription(t('Total supply of the Cryptocurrency'))
      ->setSettings([
           'unsigned' => true,
           'size' => 'big'
         ])
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -1,
        'settings' => array(
          'display_label' => TRUE,
        ),
      ))
     ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => -2,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(FALSE);

     $fields['max_supply'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Max supply')) 
      ->setDescription(t('Maximum supply of the Cryptocurrency'))
      ->setSettings([
           'unsigned' => true,
           'size' => 'big'
         ])
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -1,
        'settings' => array(
          'display_label' => TRUE,
        ),
      ))
     ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => -2,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(FALSE);

     $fields['volume'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Volume'))
      ->setDescription(t('Volume of the Cryptocurrency'))
      ->setRevisionable(TRUE)
      ->setSettings([
           'unsigned' => true,
           'size' => 'big',
           'prefix' => '$',
         ])
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -1,
        'settings' => array(
          'display_label' => TRUE,
        ),
      ))
     ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => -2,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(TRUE);

    $fields['mcap'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Market Cap'))
      ->setDescription(t('Market cap of the Cryptocurrency'))
      ->setRevisionable(TRUE)
      ->setSettings([
           'unsigned' => true,
           'size' => 'big',
           'prefix' => '$',
         ])
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -1,
        'settings' => array(
          'display_label' => TRUE,
        ),
      ))
     ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => -2,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setRevisionable(TRUE)
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Coinmarketcap entity is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 50,
      ]);

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 49,
      ]);

    return $fields;
  }

}
