<?php

namespace Drupal\coinmarketcap\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Cmc entity entities.
 *
 * @ingroup coinmarketcap
 */
interface CmcEntityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Cmc entity name.
   *
   * @return string
   *   Name of the Cmc entity.
   */
  public function getName();

  /**
   * Sets the Cmc entity name.
   *
   * @param string $name
   *   The Cmc entity name.
   *
   * @return \Drupal\coinmarketcap\Entity\CmcEntityInterface
   *   The called Cmc entity entity.
   */
  public function setName($name);

  /**
   * Gets the Cmc entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Cmc entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Cmc entity creation timestamp.
   *
   * @param int $timestamp
   *   The Cmc entity creation timestamp.
   *
   * @return \Drupal\coinmarketcap\Entity\CmcEntityInterface
   *   The called Cmc entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Cmc entity published status indicator.
   *
   * Unpublished Cmc entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Cmc entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Cmc entity.
   *
   * @param bool $published
   *   TRUE to set this Cmc entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\coinmarketcap\Entity\CmcEntityInterface
   *   The called Cmc entity entity.
   */
  public function setPublished($published);

  /**
   * Gets the Cmc entity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Cmc entity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\coinmarketcap\Entity\CmcEntityInterface
   *   The called Cmc entity entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Cmc entity revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Cmc entity revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\coinmarketcap\Entity\CmcEntityInterface
   *   The called Cmc entity entity.
   */
  public function setRevisionUserId($uid);

}
