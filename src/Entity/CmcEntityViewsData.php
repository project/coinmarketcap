<?php

namespace Drupal\coinmarketcap\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Cmc entity entities.
 */
class CmcEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
