<?php

namespace Drupal\coinmarketcap\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Cmc entity entities.
 *
 * @ingroup coinmarketcap
 */
class CmcEntityDeleteForm extends ContentEntityDeleteForm {


}
